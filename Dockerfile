FROM node:18.7.0-bullseye
LABEL org.opencontainers.image.authors="Roman Shchekotov (Doomie) <rom.shchekotov@gmail.com>"
RUN mkdir -p /usr/src/nuxt-app
WORKDIR /usr/src/nuxt-app
RUN apk update && apk upgrade && apk add git
COPY . /usr/src/nuxt-app
RUN pnpm install --shamefully-hoist && pnpm run build
EXPOSE 3000
ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000
CMD [ "pnpm", "start" ]
