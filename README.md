# Doomer's Frontpage
Look at the [nuxt 3 documentation](https://v3.nuxtjs.org) to learn more.

## Setup
Make sure to install the dependencies:
```bash
pnpm install --shamefully-hoist
```

## Development Server
Start the development server on [localhost](http://localhost:3000)
```bash
npm run dev
```

## Deployment
Netlify manages the production & staging deployment of this SPA.

Alternatively, feel free to deploy using the provided Dockerfile.
