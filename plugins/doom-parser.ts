export default defineNuxtPlugin(() => {
  return {
    provide: {
      /* Introducing: Doom Flavored Markdown, LOL */
      parseDFM: (msg: string) => {
        let specialRefs = {
          'gh': (user: string, name: string) => `<a href="https://github.com/${user}">${name ?? user}</a>`,
          'w': (slug: string, title: string) => `<a href="https://en.wikipedia.org/wiki/${slug}">${slug ?? title}</a>`,
        };
        let parseLinks = msg.replace(/\[(?:(\w+):)?\[([^\]]+)\]\[([^\]]+)\]\]/g, (_, spec, link, desc) => {
          if(!spec || !(spec in specialRefs)) {
            return `<a href="${link}">${desc}</a>`;
          } else {
            return specialRefs[spec](link, desc);
          }
        });
        let parseTT = parseLinks.replace(/=(\S|[^=\s][\s\S]*?[^\\ ])=/g, (_, text) => {
          return `<code>${text.replace(/\\=/g, '=')}</code>`;
        });
        return parseTT;
      }
    }
  }
});
